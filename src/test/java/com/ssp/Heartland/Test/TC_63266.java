package com.ssp.Heartland.Test;

import java.util.HashMap;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.ExtentTest;
import com.ssp.Heartland.SubModules.CreateNewBusinessQuote;
import com.ssp.Heartland.SubModules.NBPaynow;
import com.ssp.Heartland.SubModules.PerformMTAQuote;
import com.ssp.Heartland.SubModules.PerformRPMTAQuote;
import com.ssp.Heartland.SubModules.RaiseInsurerPayment;
import com.ssp.support.BaseTest;
import com.ssp.support.Log;
import com.ssp.utils.UIInteraction;
import com.ssp.utils.WaitUtils;
import com.ssp.uxp_HeartlandPages.Banking;
import com.ssp.uxp_HeartlandPages.CollectionScreen;
import com.ssp.uxp_HeartlandPages.HomePage;
import com.ssp.uxp_HeartlandPages.IncomeClearing;
import com.ssp.uxp_HeartlandPages.IncomeSummary;
import com.ssp.uxp_HeartlandPages.Login;
import com.ssp.uxp_HeartlandPages.NewQuoteScreens;
import com.ssp.uxp_HeartlandPages.PremiumDisplay;
import com.ssp.uxp_HeartlandPages.TransactionScreen;
import com.ssp.uxp_HeartlandPages.ViewClientDetails;

public class TC_63266 extends BaseTest {

  
  public void collectionDetails_btnClientCollection(WebDriver driver, HashMap<String, String> testData,
      ExtentTest extentReport) throws Exception {


    String tcId = "TC_63266_010";
    testData = getTestData(tcId);
    extentReport = addTestInfo(tcId, testData.get("Description"));
    HashMap<String, String> dynamicHashMap = new HashMap<>();

    // Create a hashmap to store all run time data generated.


    try {

      HashMap<String, String> loginDetails = getProperties();
      HashMap<String, String> config = getProperties();

      /****
       * 
       * Create instance of Page class LoginPage and call method to perform login
       */
      Login loginPage = new Login(driver, loginDetails.get("URL"), extentReport);
      loginPage.loginToSSP(loginDetails, driver, extentReport);

      HomePage homePage = new HomePage(driver, extentReport);
      homePage.searchClientUsingCode(testData, driver, extentReport);
      homePage.selectClient(driver, extentReport);
      ViewClientDetails vcd = new ViewClientDetails(driver, extentReport);
      vcd.clickCollectionbutton(driver, extentReport);
      CollectionScreen cs = new CollectionScreen(driver, extentReport);
      cs.VerifylblCollectionTypeAndReadonlyPostingDate(driver, extentReport);
      
    } catch (Exception e) {
      Log.exception(e, driver, extentReport);
    } finally {
      Log.testCaseResult(extentReport);
      Log.endTestCase(extentReport);
    }
  }
   
  public void collectionDetails_batchCollection(WebDriver driver, HashMap<String, String> testData,
      ExtentTest extentReport) throws Exception {


    String tcId = "TC_63266_020";
    testData = getTestData(tcId);
    extentReport = addTestInfo(tcId, testData.get("Description"));
    HashMap<String, String> dynamicHashMap = new HashMap<>();

    // Create a hashmap to store all run time data generated.


    try {

      HashMap<String, String> loginDetails = getProperties();
      HashMap<String, String> config = getProperties();

      /****
       * 
       * Create instance of Page class LoginPage and call method to perform login
       */
      Login loginPage = new Login(driver, loginDetails.get("URL"), extentReport);
      loginPage.loginToSSP(loginDetails, driver, extentReport);

      HomePage homePage = new HomePage(driver, extentReport);
      homePage.navigateToCollection(driver, extentReport);
      CollectionScreen cs = new CollectionScreen(driver, extentReport);
      cs.VerifylblCollectionTypeAndReadonlyPostingDate(driver, extentReport);
      cs.enterDetails(testData, "100", driver, extentReport);
      cs.clickOnOkAllocationButton(driver, extentReport);
      cs.clickEllipsisonCollectionList(driver, extentReport);
      cs.verifyViewCollectionLinkOnCollectionList(driver, extentReport);
      
      
    } catch (Exception e) {
      Log.exception(e, driver, extentReport);
    } finally {
      Log.testCaseResult(extentReport);
      Log.endTestCase(extentReport);
    }
  }
  
  public void collectionDetails_banking(WebDriver driver, HashMap<String, String> testData,
      ExtentTest extentReport) throws Exception {


    String tcId = "TC_63266_040";
    testData = getTestData(tcId);
    extentReport = addTestInfo(tcId, testData.get("Description"));
    HashMap<String, String> dynamicHashMap = new HashMap<>();

    // Create a hashmap to store all run time data generated.


    try {

      HashMap<String, String> loginDetails = getProperties();
      HashMap<String, String> config = getProperties();

      /****
       * 
       * Create instance of Page class LoginPage and call method to perform login
       */
      Login loginPage = new Login(driver, loginDetails.get("URL"), extentReport);
      loginPage.loginToSSP(loginDetails, driver, extentReport);

      HomePage homePage = new HomePage(driver, extentReport);
      homePage.navigateToBanking(driver, extentReport);
      Banking bank = new Banking(driver);
      bank.selectBranch(testData, driver, extentReport);
      bank.clickFind(driver, extentReport);
      bank.markSRPAndClickBank(driver, extentReport);
      CollectionScreen cs = new CollectionScreen(driver, extentReport);
      cs.VerifylblCollectionTypeAndReadonlyPostingDate(driver, extentReport);
     
    } catch (Exception e) {
      Log.exception(e, driver, extentReport);
    } finally {
      Log.testCaseResult(extentReport);
      Log.endTestCase(extentReport);
    }
  }
  
  public void collectionDetails_PayNow(WebDriver driver, HashMap<String, String> testData,
      ExtentTest extentReport) throws Exception {


    String tcId = "TC_63266_030";
    testData = getTestData(tcId);
    extentReport = addTestInfo(tcId, testData.get("Description"));
    HashMap<String, String> dynamicHashMap = new HashMap<>();

    // Create a hashmap to store all run time data generated.


    try {

      HashMap<String, String> loginDetails = getProperties();
      HashMap<String, String> config = getProperties();

      /****
       * 
       * Create instance of Page class LoginPage and call method to perform login
       */
      Login loginPage = new Login(driver, loginDetails.get("URL"), extentReport);
      loginPage.loginToSSP(loginDetails, driver, extentReport);

      HomePage homePage = new HomePage(driver, extentReport);
      homePage.searchClientUsingCode(testData, driver, extentReport);
      homePage.selectClient(driver, extentReport);
      CreateNewBusinessQuote nq=new CreateNewBusinessQuote();
      nq.createNewBusiness(driver, dynamicHashMap, extentReport, false);
      NewQuoteScreens quotesPage = new NewQuoteScreens(driver, extentReport);
      quotesPage.saveQuote(driver, extentReport);
      quotesPage.switchOutOfForm(driver);

      PremiumDisplay premiumPage = new PremiumDisplay(driver, extentReport);
      Log.softAssertThat(premiumPage.verifyPremiumPage(),
          "Summary of Cover Page Successfully loaded", "Summary of cover page not loaded", driver,
          extentReport, true);
      premiumPage.selectPayNow(driver, extentReport);
      String insurerName = premiumPage.getInsurerName(driver, extentReport);
      dynamicHashMap.put("Insurer Name", insurerName);
      premiumPage.confirmQuote(driver, extentReport);
      CollectionScreen collecton = new CollectionScreen(driver, extentReport);
      collecton.VerifylblCollectionTypeAndReadonlyPostingDate(driver, extentReport);
      collecton.enterDetailsForPayNow(testData, driver, extentReport, false);
      UIInteraction.acceptAlert(driver);
      driver.switchTo().defaultContent();            
    
    } catch (Exception e) {
      Log.exception(e, driver, extentReport);
    } finally {
      Log.testCaseResult(extentReport);
      Log.endTestCase(extentReport);
    }
  }
  
  public void collectionDetails_InsurerPayment(WebDriver driver, HashMap<String, String> testData,
      ExtentTest extentReport) throws Exception {


    String tcId = "TC_63266_050";
    testData = getTestData(tcId);
    extentReport = addTestInfo(tcId, testData.get("Description"));
    HashMap<String, String> dynamicHashMap = new HashMap<>();

    // Create a hashmap to store all run time data generated.


    try {

      HashMap<String, String> loginDetails = getProperties();
      HashMap<String, String> config = getProperties();

      /****
       * 
       * Create instance of Page class LoginPage and call method to perform login
       */
      Login loginPage = new Login(driver, loginDetails.get("URL"), extentReport);
      loginPage.loginToSSP(loginDetails, driver, extentReport);
            HomePage homePage = new HomePage(driver, extentReport);
      homePage.searchClientUsingCode(testData, driver, extentReport);
      homePage.selectClient(driver, extentReport);
      NBPaynow nb = new NBPaynow();
      nb.createNewBusiness(driver, dynamicHashMap, extentReport, false, false);
         
      
      PerformRPMTAQuote mta = new PerformRPMTAQuote();
      mta.performMTA(driver, dynamicHashMap.get("policyNo"), dynamicHashMap, extentReport);     
      NewQuoteScreens quotesPage = new NewQuoteScreens(driver, extentReport);
      quotesPage.saveQuote(driver, extentReport);
      quotesPage.switchOutOfForm(driver);
      PremiumDisplay premiumPage = new PremiumDisplay(driver, extentReport);
      Log.softAssertThat(premiumPage.verifyPremiumPage(),
          "Summary of Cover Page Successfully loaded", "Summary of cover page not loaded", driver,
          extentReport, true);
      String insurerName = premiumPage.getInsurerName(driver, extentReport);
      dynamicHashMap.put("Insurer Name", insurerName);
      premiumPage.selectInvoice(driver, extentReport);
      premiumPage.confirmQuote(driver, extentReport);
      

      
      /*-----Return-MTA Done*/
      
      homePage.navigateToTransaction(driver, extentReport);
      TransactionScreen searchPage = new TransactionScreen(driver, extentReport).get();
      searchPage.uncheckOutstandingCheckBox(driver, extentReport);
      searchPage.searchViaPolicyNo(driver, dynamicHashMap.get("policyNo"), extentReport);
      searchPage.navigateToTransactionsTab(driver, extentReport);
      searchPage.searchViaTransactionRef("SEC%", driver, extentReport);
      searchPage.performSearch(driver, extentReport);
      String docRef = searchPage.getTransactionRef(driver, extentReport);
      dynamicHashMap.put("docRef", docRef);
        RaiseInsurerPayment ip = new RaiseInsurerPayment(); 
     // setting client through insurer payment 
        ip.payInsurerWithCommAdj(driver, dynamicHashMap.get("Insurer Name"), dynamicHashMap,testData, extentReport, true, false);
      
        CollectionScreen cs
        = new CollectionScreen(driver, extentReport);    
        cs.VerifylblCollectionTypeAndReadonlyPostingDate(driver, extentReport);
    } catch (Exception e) {
      Log.exception(e, driver, extentReport);
    } finally {
      Log.testCaseResult(extentReport);
      Log.endTestCase(extentReport);
    }
  }

}